const LOAD = 'redux-example/message/LOAD';
const LOAD_SUCCESS = 'redux-example/message/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/message/LOAD_FAIL';

const LOAD_KEYS = 'redux-example/message/LOAD_KEYS';
const LOAD_KEYS_SUCCESS = 'redux-example/message/LOAD_KEYS_SUCCESS';
const LOAD_KEYS_FAIL = 'redux-example/message/LOAD_KEYS_FAIL';

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true,
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        items: action.result
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };

    case LOAD_KEYS:
      return {
        ...state,
      };
    case LOAD_KEYS_SUCCESS:
      return {
        ...state,
        keys: action.result
      };
    case LOAD_KEYS_FAIL:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/messages')
  };
}

export function loadKeys() {
  return {
    types: [LOAD_KEYS, LOAD_KEYS_SUCCESS, LOAD_KEYS_FAIL],
    promise: (client) => client.get('/messages/keys')
  };
}
