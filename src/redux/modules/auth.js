const LOAD = 'redux-example/auth/LOAD';
const LOAD_SUCCESS = 'redux-example/auth/LOAD_SUCCESS';
const LOAD_FAIL = 'redux-example/auth/LOAD_FAIL';

const LOAD_HISTORY = 'redux-example/auth/LOAD_HISTORY';
const LOAD_HISTORY_SUCCESS = 'redux-example/auth/LOAD_HISTORY_SUCCESS';
const LOAD_HISTORY_FAIL = 'redux-example/auth/LOAD_HISTORY_FAIL';

const LOGOUT = 'redux-example/auth/LOGOUT';
const LOGOUT_SUCCESS = 'redux-example/auth/LOGOUT_SUCCESS';
const LOGOUT_FAIL = 'redux-example/auth/LOGOUT_FAIL';

const SAVE_USER = 'redux-example/auth/SAVE_USER';

const initialState = {
  loaded: false,
  user: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true,
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        user: action.result.user,
        loggedOut: false
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOGOUT:
      return {
        ...state,
        loggingOut: true
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        loggingOut: false,
        user: null,
        loggedOut: true
      };
    case LOGOUT_FAIL:
      return {
        ...state,
        loggingOut: false,
        logoutError: action.error
      };
    case SAVE_USER:
      return {
        ...state,
        user: action.user.user
      };
    case LOAD_HISTORY:
      return {
        ...state,
        loadedHistory: false
      };
    case LOAD_HISTORY_SUCCESS:
      return {
        ...state,
        history: action.result,
        loadedHistory: true
      };
    case LOAD_HISTORY_FAIL:
      return {
        ...state,
        loadedHistory: false
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.auth && globalState.auth.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/users/login')
  };
}

export function loadHistory() {
  return {
    types: [LOAD_HISTORY, LOAD_HISTORY_SUCCESS, LOAD_HISTORY_FAIL],
    promise: (client) => client.get('/users/auth_history')
  };
}

export function logout() {
  return {
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAIL],
    promise: (client) => client.post('/users/logout')
  };
}

export function saveUser(user) {
  return {
    type: SAVE_USER,
    user: user
  };
}
