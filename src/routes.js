import React from 'react';
import {IndexRoute, Route} from 'react-router';
import {
  App,
  Login,
  NotFound,
  Registration,
  ForgotPassword,
  ChangePassword,
  Home,
  Profile,
  Activation,
  Message
} from 'containers';
import { isLoaded as isAuthLoaded, load as loadAuth } from 'redux/modules/auth';

export default (store) => {
  const requireLogin = (nextState, replace, cb) => {
    function checkAuth() {
      // if (nextState.params && nextState.params.model) {
      //   nextState.model = encodeURIComponent(nextState.model);
      // }
      // console.log('----nextState', nextState);
      const { auth: { user }} = store.getState();
      if (!user) {
        // oops, not logged in, so can't be here!
        replace('/');
      }
      cb();
    }

    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };

  /**
   * Please keep routes in alphabetical order
   */
  return (
    <Route path="/" component={App} breadcrumbIgnore>
      { /* Home (main) route */ }
      <IndexRoute component={Home} />
      <Route path="/login" component={Login} />
      <Route path="/registration" component={Registration} />
      <Route path="/forgot_password" component={ForgotPassword} />
      <Route path="/change_password" component={ChangePassword} />
      <Route path="/activation" component={Activation} />
      <Route path="/message" component={Message} />

      <Route path="/profile" component={Profile} onEnter={requireLogin} />
      { /* Catch all route */ }
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
