import React from 'react';
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup';

export default ({ input, ...rest }) => (
  <RadioButtonGroup
    {...input} {...rest}
    valueSelected={input.value}
    onChange={(event, value) => input.onChange(value)}
    defaultSelected="not_light"
    style={{display: 'flex', flexDirection: 'row'}}
  />
);
