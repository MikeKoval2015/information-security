import React from 'react';
import AutoComplete from 'material-ui/AutoComplete';

export default ({label, input, source, meta: {touched, error}, ...restProps}) => (
  <div className={(error && touched ? ' has-error' : '')}>
    <AutoComplete
      hintText={label}
      onUpdateInput={input.onChange}
      searchText={input.value}
      dataSource={source}
      {...restProps}
    />
  </div>
);
