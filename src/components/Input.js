import React from 'react';
import TextField from 'material-ui/TextField';
import {FormattedMessage} from 'react-intl';

const intlIfObject = (value) => {
  if (typeof value === 'object') {
    return <FormattedMessage {...value} />;
  }
  return value;
};

export default ({
  type, inline = false, hintText = '', placeholder = '', input, rows = 1, meta: {asyncValidating, touched, error}, ...restProps
}) => {
  const textField = (
    <TextField
      type={type}
      {...input}
      floatingLabelText={hintText}
      hintText={placeholder}
      errorText={touched && (error ? intlIfObject(error) : null)}
      rows={rows}
      {...restProps}
    />
  );

  if (inline) {
    return (
      <span>
        <span className={(error && touched ? ' has-error' : '') + ' ' + asyncValidating ? 'async-validating' : ''}>
          {textField}
        </span>
      </span>
    );
  }

  return (
    <div>
      <div className={(error && touched ? ' has-error' : '') + ' ' + asyncValidating ? 'async-validating' : ''}>
        {textField}
      </div>
    </div>
  );
};
