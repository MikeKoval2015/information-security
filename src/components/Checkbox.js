import React from 'react';
import Checkbox from 'material-ui/Checkbox';

export default ({ input, meta: {touched, error}, ...rest }) => (
  <span className={(error && touched ? ' has-error' : '') + ' '}>
        <Checkbox
          onCheck={input.onChange}
          checked={!!input.value}
          {...rest}
        />
    </span>
);
