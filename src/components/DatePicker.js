import React from 'react';
import DatePicker from 'material-ui/DatePicker';

export default ({input, meta: {touched, error}, ...restProps}) => {
  return (
    <div className={(error && touched ? ' has-error' : '')}>
      <DatePicker
        onChange={(__, value) => input.onChange(value)}
        value={input.value}
        {...restProps}
      />
    </div>
  );
};
