import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

export default () =>
  <div style={{position: 'absolute', left: '50%', top: '50%', zIndex: 5000}}>
    <CircularProgress size={80} thickness={5} />
  </div>;
