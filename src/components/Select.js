import React from 'react';
import SelectField from 'material-ui/SelectField';
import {FormattedMessage} from 'react-intl';

const intlIfObject = (value) => {
  if (typeof value === 'object') {
    return <FormattedMessage {...value} />;
  }
  return value;
};
export default ({ input, label, meta: { touched, error }, children, ...custom }) => (
  <SelectField
    floatingLabelText={label}
    errorText={touched && (error ? intlIfObject(error) : null)}
    {...input}
    onChange={(event, index, value) => input.onChange(value)}
    children={children}
    {...custom}/>
);
