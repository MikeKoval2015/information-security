import React, {Component, PropTypes} from 'react';
import ReactPaginate from 'react-paginate';
import {injectIntl, intlShape, defineMessages} from 'react-intl';

const messages = defineMessages({
  Previous: {
    id: 'Pagination.Previous',
    defaultMessage: 'previous'
  },
  Next: {
    id: 'Pagination.Next',
    defaultMessage: 'next'
  }
});

@injectIntl
export default class Pagination extends Component {
  static propTypes = {
    pageNum: PropTypes.number.isRequired,
    handlePageClick: PropTypes.func.isRequired,
    intl: intlShape.isRequired
  };

  render() {
    const {pageNum, handlePageClick, intl} = this.props;
    return (
      <div>
        { !!pageNum && pageNum > 1 &&
        <ReactPaginate
          previousLabel={intl.formatMessage(messages.Previous)}
          nextLabel={intl.formatMessage(messages.Next)}
          breakLabel={<a href="">...</a>}
          breakClassName="break-me"
          pageNum={pageNum}
          marginPagesDisplayed={2}
          pageRangeDisplayed={5}
          clickCallback={handlePageClick}
          containerClassName="pagination"
          subContainerClassName="pages pagination"
          activeClassName="active" />
        }
      </div>
    );
  }
}
