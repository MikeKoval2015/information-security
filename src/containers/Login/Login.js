﻿import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import LoginForm from './LoginForm';
import {defineMessages, intlShape, injectIntl} from 'react-intl';

const messages = defineMessages({
  Title: {
    id: 'Login.Title',
    defaultMessage: 'Login'
  }
});

@injectIntl
export default class Login extends Component {
  static propTypes = {
    user: PropTypes.object,
    logout: PropTypes.func,
    saveUser: PropTypes.func,
    initialize: PropTypes.func,
    intl: intlShape.isRequired
  };

  render() {
    const {intl} = this.props;
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'stretch',
        alignContent: 'center',
        height: 'calc(100vh - 220px)'
      }}>
        <Helmet title={intl.formatMessage(messages.Title)} />
        <LoginForm />
      </div>
    );
  }
}
