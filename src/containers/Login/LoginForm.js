﻿import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import config from '../../config.js';
import * as authActions from '../../redux/modules/auth';
import {bindActionCreators} from 'redux';
import LoginValidation from './LoginValidation';
import {Input} from 'components';
import { push } from 'react-router-redux';
import {FormattedMessage, defineMessages, injectIntl, intlShape} from 'react-intl';

const messages = defineMessages({
  Email: {
    id: 'LoginForm.Email',
    defaultMessage: 'E-mail'
  },
  Password: {
    id: 'LoginForm.Password',
    defaultMessage: 'Password'
  },
  LogIn: {
    id: 'LoginForm.LogIn',
    defaultMessage: 'Log In'
  },
  LogOut: {
    id: 'LoginForm.LogOut',
    defaultMessage: 'Log out'
  },
  ForgotPassword: {
    id: 'LoginForm.ForgotPassword',
    defaultMessage: 'Forgot password?'
  },
  SignUp: {
    id: 'LoginForm.SignUp',
    defaultMessage: 'Sign Up'
  }
});

@injectIntl
@reduxForm({
  form: 'loginForm',
  enableReinitialize: true,
  validate: LoginValidation
})
@connect(
    state => ({
      form: state.form.loginForm,
      user: state.auth.user
    }),
  dispatch => bindActionCreators({...authActions, pushState: push}, dispatch)
)
export default class Login extends Component {
  static propTypes = {
    user: PropTypes.object,
    logout: PropTypes.func,
    saveUser: PropTypes.func,
    initialize: PropTypes.func,
    isPopUp: PropTypes.bool,
    intl: intlShape.isRequired
  };

  static defaultProps = {
    isPopUp: false
  };

  submit = (data) => {
    return Promise.resolve()
      .then(() => {
        const apiPath = 'http://' + config.apiHost + ':' + config.apiPort;
        return fetch(apiPath + '/users/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
          mode: 'cors',
          body: JSON.stringify(data)
        })
          .then((response) => {
            return response.json()
              .then(json => {
                json.code = response.status;
                return json;
              });
          })
          .then((responseJson) => {
            if (responseJson.code === 429) {
              // todo
              throw new SubmissionError({_error: 'Too many attempts to login with wrong credentials, please try again after a minute'});
            }
            if (responseJson && responseJson.info && responseJson.info.message && responseJson.info.message !== 'Successfully logged in') {
              throw new SubmissionError({_error: responseJson.info.message});
            }
            if (!responseJson.user) {
              throw new SubmissionError({_error: responseJson.info.message});
            }
            this.props.saveUser(responseJson);
            this.props.initialize({});
          })
          .catch((err) => {
            console.log('error', err);
            throw new SubmissionError({_error: err && err.errors && err.errors._error});
          });
      });
  };

    render() {
      /* eslint-disable */
      const {
        handleSubmit,
        error,
        user,
        logout,
        invalid,
        submitting,
        isPopUp,
        intl,
        onForgotPasswordClick,
        pushState
      } = this.props;
      /* eslint-disable */

      if (user) {
        return (
          <div className="container">
            <p>
              <FormattedMessage
                id="Login.YouAreCurrentlyLoggedInAs"
                defaultMessage="You are currently logged in as"
              />&nbsp;
              {user && user.email}
            </p>

            <div style={{textAlign: 'center'}}>
              <RaisedButton
                onClick={logout}
                primary
                className="button_without_shadow"
                label={intl.formatMessage(messages.LogOut)}
              />
            </div>
          </div>
        )
      }

      return (
        <div style={!isPopUp ? {display: 'flex', alignSelf: 'center', width: 360} : {width: 360}}>
          <form
            onSubmit={handleSubmit(this.submit)}
            style={{
              display: 'block',
              width: '100%',
              border: '1px solid #efeff0',
              background: '#fff',
              boxShadow: '0px 3px 5px 0px rgba(0,0,0,0.05)'
            }}>
            <div style={{padding: 25, paddingTop: 0}}>
              {!isPopUp && <div>
                <h2>
                  <FormattedMessage
                    id="Login.Header"
                    defaultMessage="Login"
                  />
                </h2>
              </div>}
              <Field
                name="email"
                hintText={intl.formatMessage(messages.Email)}
                type="text"
                fullWidth
                component={Input} />

              <Field
                name="password"
                hintText={intl.formatMessage(messages.Password)}
                type="password"
                fullWidth
                component={Input} />

              <div>
                {error && <div className="text-danger">{error}</div>}
              </div>

              <div style={{display: 'flex', flexDirection: 'row', marginTop: 25}}>
                <div style={{display: 'flex', flex: 1}}>

                </div>
                <div style={{display: 'flex', flex: 1}}>
                  <RaisedButton
                    type="submit"
                    label={intl.formatMessage(messages.LogIn)}
                    className="button_without_shadow"
                    fullWidth
                    primary
                    disabled={invalid || submitting}
                  />
                </div>
              </div>
            </div>
            <div style={{display: 'flex', flexDirection: 'row'}}>
              <div style={{display: 'flex', flex: 1, borderTop: '1px solid #efeff0', borderRight: '1px solid #efeff0'}}>
                <RaisedButton
                  href="#"
                  onClick={(event) => {
                    event.preventDefault();
                    onForgotPasswordClick ? onForgotPasswordClick() : pushState('forgot_password')
                  }}
                  label={intl.formatMessage(messages.ForgotPassword)}
                  className="button_without_shadow"
                  style={{display: 'flex', flex: 1, height: 55}}
                  labelColor="#979797"
                  backgroundColor="#fff"
                />
              </div>
              <div style={{display: 'flex', flex: 1, borderTop: '1px solid #efeff0'}}>
                <RaisedButton
                  href="/registration"
                  label={intl.formatMessage(messages.SignUp)}
                  className="button_without_shadow"
                  style={{display: 'flex', flex: 1, height: 55}}
                  labelColor="#ffc733"
                />
              </div>
            </div>
          </form>
        </div>
      );
    }
}
