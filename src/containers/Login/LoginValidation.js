export default (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = {id: 'General.Required', defaultMessage: 'Required'};
  }

  if (!values.password) {
    errors.password = {id: 'General.Required', defaultMessage: 'Required'};
  }

  return errors;
};
