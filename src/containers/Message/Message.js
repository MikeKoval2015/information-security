﻿import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import * as messageActions from '../../redux/modules/message';
import {bindActionCreators} from 'redux';
import {Input} from 'components';
import { push } from 'react-router-redux';
import config from '../../config';
import {FormattedMessage, defineMessages, injectIntl, intlShape} from 'react-intl';
import rsa from '../../../api/server/helpers/rsa';
import moment from 'moment';
import TextField from 'material-ui/TextField';

const messages = defineMessages({
  Message: {
    id: 'MessageForm.Message',
    defaultMessage: 'Message'
  },
  Send: {
    id: 'MessageForm.Send',
    defaultMessage: 'Send'
  }
});

@injectIntl
@reduxForm({
  form: 'MessageForm',
  enableReinitialize: true
})
@connect(
  state => ({
    form: state.form.MessageForm,
    items: state.message.items,
    keys: state.message.keys,
  }),
  dispatch => bindActionCreators({...messageActions, pushState: push}, dispatch)
)
export default class Message extends Component {
  static propTypes = {
    initialize: PropTypes.func,
    intl: intlShape.isRequired,
    load: PropTypes.func,
    loadKeys: PropTypes.func,
    // items: PropTypes.array,
    keys: PropTypes.object
  };

  componentDidMount() {
    this.props.load();
    this.props.loadKeys();
  }

  submit = (data) => {
    const encrypted = this.encrypt((data && data.message) || '');
    const requestData = {
      encrypted
    };

    const apiPath = 'http://' + config.apiHost + ':' + config.apiPort;
    return fetch(apiPath + '/messages', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      credentials: 'include',
      mode: 'cors',
      body: JSON.stringify(requestData)
    })
      .then((response) => {
        return response.json()
          .then(json => {
            json.code = response.status;
            return json;
          });
      })
      .then((responseJson) => {
        console.log('---responseJson', responseJson);
        this.props.initialize({});
        this.props.load();
      })
      .catch((err) => {
        console.log('error', err);
        throw new SubmissionError({_error: err && err.errors && err.errors._error});
      });
  };

  encrypt = value => {
    const { keys } = this.props;
    const array = rsa.toCharArray(value);
    const encryptedArray = [];
    for (const ee of array) {
      const number = rsa.encrypt({ e: keys.e.value, n: keys.n.value }, ee);
      encryptedArray.push(number.toString());
    }
    return encryptedArray.join('|');
  };

  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      error,
      invalid,
      submitting,
      intl,
      pushState,
      items,
      keys
    } = this.props;
    /* eslint-disable */

    const encryptedValue = this.encrypt((keys && this.props.form && this.props.form.values && this.props.form.values.message) || '');

    return (
      <div>
  <form
    onSubmit={handleSubmit(this.submit)}>
      <div>
        <div>
          <h2>
            <FormattedMessage
              id="Login.Header"
              defaultMessage="Login"
            />
          </h2>
        </div>
        <p>
          Encrypt and send message | e: {keys && keys.e.value} n: {keys && keys.n.value}
        </p>
      <Field
        name="message"
        hintText={intl.formatMessage(messages.Message)}
        type="text"
        fullWidth
        component={Input} />

        <TextField
          multiLine={true}
          rows={5}
          value={encryptedValue}
          fullWidth
        />

          <div>
          {error && <div className="text-danger">{error}</div>}
      </div>

        <div style={{display: 'flex', flexDirection: 'row', marginTop: 25}}>
      <div style={{display: 'flex', flex: 1}}>

      </div>
        <div style={{display: 'flex', flex: 1}}>
          <RaisedButton
            type="submit"
            label={intl.formatMessage(messages.Send)}
            className="button_without_shadow"
            fullWidth
            primary
            disabled={invalid || submitting}
          />
          </div>
        </div>
        </div>
      </form>
        {items && items.map((item, index)=> (
          <div key={index}>{moment(item.date).format('YYYY-MM-DD HH:mm:ss')}&nbsp;{item.text}</div>
        ))}
      </div>
  );
  }
}
