const validate = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = {id: 'General.Required', defaultMessage: 'Required'};
  }
  if (values.email && !/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(values.email)) {
    errors.email = {id: 'General.WrongFormat', defaultMessage: 'Wrong format'};
  }

  return errors;
};


export default validate;
