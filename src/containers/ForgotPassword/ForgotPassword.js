import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {bindActionCreators} from 'redux';
import * as authActions from 'redux/modules/auth';
import {FormattedMessage, defineMessages, injectIntl, intlShape} from 'react-intl';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import { replace as replaceRoute } from 'react-router-redux';
import { Input } from 'components';
import config from '../../config.js';
import RaisedButton from 'material-ui/RaisedButton';
import ForgotPasswordValidation from './ForgotPasswordValidation';
import { push } from 'react-router-redux';

const messages = defineMessages({
  Title: {
    id: 'ForgotPassword.Title',
    defaultMessage: 'Forgot password'
  },
  Email: {
    id: 'ForgotPassword.Email',
    defaultMessage: 'Type your registration email'
  },
  SendEmail: {
    id: 'ForgotPassword.SendEmail',
    defaultMessage: 'Reset Password'
  },
  Login: {
    id: 'ForgotPassword.Login',
    defaultMessage: 'Login'
  },
  SignUp: {
    id: 'ForgotPassword.SignUp',
    defaultMessage: 'Sign Up'
  }
});

@injectIntl
@reduxForm({
  form: 'forgotPassword',
  enableReinitialize: true,
  validate: ForgotPasswordValidation
})
@connect(
  state => ({user: state.auth.user}),
  dispatch => bindActionCreators({...authActions, replaceRoute, pushState: push}, dispatch)
)
export default class ForgotPassword extends Component {
  static propTypes = {
    user: PropTypes.object,
    login: PropTypes.func,
    logout: PropTypes.func,
    saveUser: PropTypes.func,
    replaceRoute: PropTypes.func,
    location: PropTypes.object,
    initialize: PropTypes.func,
    intl: intlShape.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {isSent: false};
  }

  submit = (data) => {
    this.setState({isSent: false});
    return Promise.resolve()
      .then(() => {
        const apiPath = 'http://' + config.apiHost + ':' + config.apiPort;

        return fetch(apiPath + '/users/forgot_password', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
          mode: 'cors',
          body: JSON.stringify(data)
        })
          .then((response) => response.json())
          .then((response) => {
            if (response.code === 500) {
              throw new SubmissionError({_error: response.msg});
            } else {
              this.props.initialize({
                email: ''
              });
              this.setState({isSent: true});
            }
          })
          .catch((err) => {
            console.log('error', err);
            throw new SubmissionError({_error: err.errors && err.errors._error});
          });
      });
  };

  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      invalid,
      error,
      submitting,
      intl,
      onLoginClick,
      pushState
    } = this.props;
    /* eslint-disable */

    const styles = require('./ForgotPassword.scss');

    return (
      <div>
        <Helmet title={intl.formatMessage(messages.Title)} />
        <div style={{width: 360, margin: '0 auto'}}>
          <form onSubmit={handleSubmit(this.submit)} className={styles.mTLg}>
            <div style={{padding: '0px 25px 25px'}}>
              <h3>
                <FormattedMessage
                  id="ForgotPassword.Header"
                  defaultMessage="Lost password?"
                />
              </h3>
              <Field
                name="email"
                hintText={intl.formatMessage(messages.Email)}
                type="email"
                fullWidth
                component={Input}
              />
              <div style={{textAlign: 'center', marginTop: 30}}>
                {error && <div className="text-danger">{error}</div>}
                {this.state.isSent && <div className="text-success">
                  <FormattedMessage
                    id="ForgotPassword.RequestSentMessage"
                    defaultMessage="If a valid email address has been provided, a reset link shall be sent to you.If you do not see the email in a few minutes, please check your spam filter for a message from info@partsbookstore.com"
                  />
                </div>}
              </div>
              <div style={{display: 'flex', flexDirection: 'row', marginTop: 25}}>
                <div style={{display: 'flex', flex: 1}}>
                </div>
                <div style={{display: 'flex', flex: 1}}>
                  <RaisedButton
                    type="submit"
                    fullWidth
                    className="button_without_shadow"
                    label={intl.formatMessage(messages.SendEmail)}
                    disabled={invalid || submitting}
                  />
                </div>
              </div>
            </div>
            <div style={{display: 'flex', flexDirection: 'row'}}>
              <div style={{display: 'flex', flex: 1, borderTop: '1px solid #efeff0', borderRight: '1px solid #efeff0'}}>
                <RaisedButton
                  href="#"
                  onClick={(event) => {
                    event.preventDefault();
                    onLoginClick ? onLoginClick() : pushState('/login')
                  }}
                  label={intl.formatMessage(messages.Login)}
                  className="button_without_shadow"
                  style={{display: 'flex', flex: 1, height: 55}}
                  labelColor="#979797"
                  backgroundColor="#fff"
                />
              </div>
              <div style={{display: 'flex', flex: 1, borderTop: '1px solid #efeff0'}}>
                <RaisedButton
                  href="/registration"
                  label={intl.formatMessage(messages.SignUp)}
                  className="button_without_shadow"
                  style={{display: 'flex', flex: 1, height: 55}}
                  labelColor="#ffc733"
                />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
