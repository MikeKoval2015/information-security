﻿import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import config from '../../config.js';
import * as authActions from '../../redux/modules/auth';
import RegistrationValidation from './RegistrationValidation';
import asyncValidate from './AsyncEmailValidation';
import {Input} from 'components';
import { browserHistory } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import {defineMessages, injectIntl, intlShape} from 'react-intl';
// import AddCircleIcon from 'ma'

const messages = defineMessages({
  Title: {
    id: 'Registration.Title',
    defaultMessage: 'Sign up'
  },
  Email: {
    id: 'Registration.Email',
    defaultMessage: 'Email *'
  },
  Password: {
    id: 'Registration.Password',
    defaultMessage: 'Password *'
  },
  RepeatPassword: {
    id: 'Registration.RepeatPassword',
    defaultMessage: 'Repeat password *'
  },
  Next: {
    id: 'Registration.Next',
    defaultMessage: 'Next'
  },
  Back: {
    id: 'Registration.Back',
    defaultMessage: 'Back'
  },
  Finish: {
    id: 'Registration.Finish',
    defaultMessage: 'Finish'
  },
});

const apiPath = 'http://' + config.apiHost + ':' + config.apiPort;

@injectIntl
@reduxForm({
  form: 'registrationForm',
  enableReinitialize: true,
  validate: RegistrationValidation,
  asyncValidate,
  asyncBlurFields: [ 'email' ]
})
@connect(
  state => ({
    user: state.auth.user,
    form: state.form.registrationForm,
  }),
  {
    ...authActions,
  }
)
export default class Registration extends Component {
  static propTypes = {
    initialize: PropTypes.func,
    form: PropTypes.object,
    intl: intlShape.isRequired,
    user: PropTypes.object,
  };

  submit = (data) => {
    return Promise.resolve()
      .then(() => {
        return fetch(apiPath + '/users/register', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
          mode: 'cors',
          body: JSON.stringify(data)
        })
          .then((response) => response.json())
          .then((responseJson) => {
            if (!responseJson || !responseJson.success) {
              throw new SubmissionError(responseJson);
            }
            this.props.initialize({});
            browserHistory.push('/');
          })
          .catch((err) => {
            console.log('error', err);
            throw new SubmissionError(err && err.errors);
          });
      });
  };

  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      error,
      invalid,
      submitting,
      form,
      intl,
      user
    } = this.props;
    /* eslint-disable */
    const contentStyle = { margin: '0 16px' };

    return (
      <div>
        <Helmet title={intl.formatMessage(messages.Title)}/>
        <div className="container">
          <div>
            <h2>
              {intl.formatMessage(messages.Title)}
            </h2>
          </div>
          <form>
            <div style={{ width: '100%', maxWidth: 900, margin: 'auto' }}>
              <div style={contentStyle}>
                <div>
                  <div>
                    <Field
                      name="email"
                      hintText={intl.formatMessage(messages.Email)}
                      type="email"
                      fullWidth
                      component={Input} />

                    <Field
                      name="password"
                      hintText={intl.formatMessage(messages.Password)}
                      type="password"
                      fullWidth
                      component={Input} />

                    <Field
                      name="password2"
                      hintText={intl.formatMessage(messages.RepeatPassword)}
                      type="password"
                      fullWidth
                      component={Input} />
                  </div>
                  <div>
                    {error && <div className="text-danger" style={{ marginTop: 10 }}>{error}</div>}
                  </div>
                  <div style={{ marginTop: 65, marginBottom: 20, textAlign: 'right' }}>
                    <RaisedButton
                      label={intl.formatMessage(messages.Finish)}
                      primary
                      className="button_without_shadow"
                      disabled={!!form.asyncErrors || submitting}
                      onTouchTap={handleSubmit(this.submit)}
                    />
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
