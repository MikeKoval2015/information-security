const validate = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = {id: 'General.Required', defaultMessage: 'Required'};
  }
  if (values.email && !/^(([^<>()\[\]\\.,;:\s@']+(\.[^<>()\[\]\\.,;:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(values.email)) {
    errors.email = {id: 'General.WrongFormat', defaultMessage: 'Wrong format'};
  }

  const passHasNumber = values && values.password && /\d/g.test(values.password);
  const passHasUpperCase = values && values.password && /[A-Z]/g.test(values.password);
  const passHasLowerCase = values && values.password && /[a-z]/g.test(values.password);
  const passHasSpecSymbols = values && values.password && /[^a-zA-Z0-9]/g.test(values.password);

  const passStrength = (passHasNumber + passHasUpperCase + passHasLowerCase + passHasSpecSymbols) || 0;

  if (passStrength < 2) {
    errors.password = {
      id: 'General.PasswordsMustUseAtLeast2Types',
      defaultMessage: 'Passwords must use at least 2 of the four available character types: lowercase letters, uppercase letters, numbers, and symbols'
    };
  }

  if (!values.password) {
    errors.password = {id: 'General.Required', defaultMessage: 'Required'};
  }

  if (values.password && values.password.length < 6) {
    errors.password = {
      id: 'General.PasswordMustHaveAtLeastSixCharacters',
      defaultMessage: 'Password must have at least six characters'
    };
  }

  if (!values.password2) {
    errors.password2 = {id: 'General.Required', defaultMessage: 'Required'};
  }

  if (values.password !== values.password2) {
    errors.password2 = {
      id: 'General.PasswordsMustMatch',
      defaultMessage: 'Passwords must match'
    };
  }

  return errors;
};


export default validate;
