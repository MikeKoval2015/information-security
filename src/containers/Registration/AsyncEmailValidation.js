import config from '../../config.js';

const apiPath = 'http://' + config.apiHost + ':' + config.apiPort;

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

export default (values) => {
  return sleep(1000)
    .then(() => {
      if (!values || !values.email) return false;
      return fetch(apiPath + '/users/email_exists?email=' + values.email, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        credentials: 'include',
        mode: 'cors'
      })
        .then((response) => response.json());
    })
    .then((isExist) => {
      if (isExist) {
        /* eslint-disable */
        throw { email: {id: 'General.ThisEmailIsTaken', defaultMessage: 'This email is taken'} };
        /* eslint-disable */
      }
    });
};