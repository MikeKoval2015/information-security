import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {FormattedMessage, defineMessages, injectIntl, intlShape} from 'react-intl';
import { replace as replaceRoute } from 'react-router-redux';
import config from '../../config.js';

const messages = defineMessages({
  Header: {
    id: 'Activation.Header',
    defaultMessage: 'Activate account'
  },
});

@injectIntl
@connect(
  () => ({}),
  {
    replaceRoute
  }
)
export default class ChangePassword extends Component {
  static propTypes = {
    location: PropTypes.object,
    replaceRoute: PropTypes.func,
    initialize: PropTypes.func,
    intl: intlShape.isRequired
  };

  state = {
    invalidToken: true
  };

  componentDidMount() {
    const {location} = this.props;
    const token = location.query.token;
    const apiPath = 'http://' + config.apiHost + ':' + config.apiPort;

    fetch(apiPath + '/users/activate/' + token, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      credentials: 'include',
      mode: 'cors'
    })
      .then((response) => response.json())
      .then((response) => {
        if (!response._error) {
          this.setState({
            invalidToken: false,
          });
          this.props.replaceRoute('/login');
        }
      })
      .catch((err) => {
        console.log(err);
        // throw new SubmissionError({_error: (err.errors && err.errors._error)});
      });
  }

  render() {
    /* eslint-disable */
    const {
      intl
    } = this.props;
    /* eslint-disable */

    const styles = require('./Activation.scss');
    return (
      <div className={styles.loginPage + ' container'}>
        <Helmet title={intl.formatMessage(messages.Header)} />
        <div className={'row ' + styles.mTLg}>
          <div className="col-sm-12">
            <h2>
              <FormattedMessage
                id="Activation.AccountActivation"
                defaultMessage="Account activation"
              />
            </h2>
            <div>
              {this.state.invalidToken && <div className="text-danger">
                <FormattedMessage
                  id="Activation.TokenNotFount"
                  defaultMessage="token is not found or user is already activated"
                />
              </div>}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
