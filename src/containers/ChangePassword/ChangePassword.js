import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import {FormattedMessage, defineMessages, injectIntl, intlShape} from 'react-intl';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import ChangePasswordValidation from './ChangePasswordValidation';
import { replace as replaceRoute } from 'react-router-redux';
import config from '../../config.js';
import {Input} from 'components';
import RaisedButton from 'material-ui/RaisedButton';

const messages = defineMessages({
  Password: {
    id: 'ChangePassword.Password',
    defaultMessage: 'Password'
  },
  RepeatPassword: {
    id: 'ChangePassword.RepeatPassword',
    defaultMessage: 'Repeat password'
  },
  Header: {
    id: 'ChangePassword.Header',
    defaultMessage: 'Change password'
  },
  Submit: {
    id: 'ChangePassword.Submit',
    defaultMessage: 'Submit'
  }
});

@injectIntl
@reduxForm({
  form: 'changePassword',
  validate: ChangePasswordValidation,
  enableReinitialize: true
})
@connect(
  () => ({}),
  {
    replaceRoute
  }
)
export default class ChangePassword extends Component {
  static propTypes = {
    location: PropTypes.object,
    replaceRoute: PropTypes.func,
    initialize: PropTypes.func,
    intl: intlShape.isRequired
  };

  state = {
    invalidToken: true
  };

  componentDidMount() {
    const {location, initialize} = this.props;
    const token = location.query.token;
    const apiPath = 'http://' + config.apiHost + ':' + config.apiPort;

    fetch(apiPath + '/users/get_reset_password_token/' + token, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      credentials: 'include',
      mode: 'cors'
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.code !== 500) {
          this.setState({
            ...this.state,
            invalidToken: false,
          });

          initialize({...response});
        }
      })
      .catch((err) => {
        console.log(err);
        throw new SubmissionError({_error: (err.errors && err.errors._error)});
      });
  }

  submit = (data) => {
    const {location} = this.props;
    return Promise.resolve()
      .then(() => {
        data.token = location.query.token;

        const apiPath = 'http://' + config.apiHost + ':' + config.apiPort;

        return fetch(apiPath + '/users/change_password', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          credentials: 'include',
          mode: 'cors',
          body: JSON.stringify(data)
        })
          .then((response) => response.json())
          .then((response) => {
            if (response.code === 500) {
              throw new SubmissionError({_error: response.msg});
            } else {
              this.props.replaceRoute('/login');
            }
          })
          .catch((err) => {
            throw new SubmissionError({_error: (err.errors && err.errors._error)});
          });
      });
  };

  render() {
    /* eslint-disable */
    const {
      handleSubmit,
      invalid,
      submitting,
      error,
      intl
    } = this.props;
    /* eslint-disable */

    const styles = require('./ChangePassword.scss');
    return (
      <div className={styles.loginPage + ' container'}>
        <Helmet title={intl.formatMessage(messages.Header)} />
        <div className={'row ' + styles.mTLg}>
          <div className="col-sm-12">
            <div className="panel panel-default panel-border-color panel-border-color-warning">
              <div className="panel-heading panel-heading-divider">
                  <h2>
                    <FormattedMessage
                      id="ChangePassword.ChangePassword"
                      defaultMessage="Select your new password"
                    />
                  </h2>
                </div>
                <div className="panel-body">
                  <form onSubmit={handleSubmit(this.submit)}>
                    <Field
                      name="password"
                      component={Input}
                      type="password"
                      hintText={intl.formatMessage(messages.Password)}
                      fullWidth
                      disabled={this.state.invalidToken}
                    />
                    <Field
                      name="repeatPassword"
                      component={Input}
                      type="password"
                      hintText={intl.formatMessage(messages.RepeatPassword)}
                      fullWidth
                      disabled={this.state.invalidToken}
                    />
                    <div style={{textAlign: 'center'}}>
                      {!error && this.state.invalidToken && <div className="text-danger">
                        <FormattedMessage
                          id="ChangePassword.TokenNotFount"
                          defaultMessage="token is not found or expired"
                        />
                      </div>}
                      {error && <div className="text-danger">{error}</div>}
                      <RaisedButton type="submit" className="button_without_shadow" label={intl.formatMessage(messages.Submit)} disabled={invalid || submitting} />
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    );
  }
}
