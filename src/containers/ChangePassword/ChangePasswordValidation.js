const validate = (values) => {
  const errors = {};

  const passHasNumber = values && values.password && /\d/g.test(values.password);
  const passHasUpperCase = values && values.password && /[A-Z]/g.test(values.password);
  const passHasLowerCase = values && values.password && /[a-z]/g.test(values.password);
  const passHasSpecSymbols = values && values.password && /[^a-zA-Z0-9]/g.test(values.password);

  const passStrength = (passHasNumber + passHasUpperCase + passHasLowerCase + passHasSpecSymbols) || 0;

  if (passStrength < 2) {
    errors.password = {id: 'General.PasswordsMustUseAtLeast2Types', defaultMessage: 'Passwords must use at least 2 of the four available character types: lowercase letters, uppercase letters, numbers, and symbols'};
  }

  if (!values.password) {
    errors.password = {id: 'General.Required', defaultMessage: 'Required'};
  }

  if (values.password && values.password.length < 6) {
    errors.password = {id: 'General.PasswordMustHaveAtLeastSixCharacters', defaultMessage: 'Password must have at least six characters'};
  }

  if (values.password &&
    (values.name && values.name.first && values.password && values.password.toLowerCase().indexOf(values.name.first.toLowerCase()) !== -1) ||
    (values.name && values.name.last && values.password && values.password.toLowerCase().indexOf(values.name.last.toLowerCase()) !== -1)
  ) {
    errors.password = {id: 'General.PasswordCannotContainYourName', defaultMessage: 'Passwords can’t contain your name'};
  }

  if (!values.repeatPassword) {
    errors.repeatPassword = {id: 'General.Required', defaultMessage: 'Required'};
  } else if (values.password !== values.repeatPassword) {
    errors.repeatPassword = {id: 'General.PasswordsMustMatch', defaultMessage: 'Passwords must match'};
  }

  return errors;
};


export default validate;
