import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {AutoAffix} from 'react-overlays';
import Navigation from '../App/Navigation';
import {FormattedMessage, defineMessages, injectIntl, intlShape} from 'react-intl';
import { push } from 'react-router-redux';
import * as authActions from 'redux/modules/auth';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
// import _ from 'lodash'; //eslint-disable-line
import moment from 'moment';
import {ProgressBar} from 'components';

const messages = defineMessages({
  Title: {
    id: 'Home.Title',
    defaultMessage: 'Home'
  },
  SignIn: {
    id: 'Home.SignIn',
    defaultMessage: 'Sign in'
  },
  Register: {
    id: 'Home.Register',
    defaultMessage: 'Register'
  }
});

@injectIntl
@connect(
  state => ({
    user: state.auth.user,
    history: state.auth.history,
    loadedHistory: state.auth.loadedHistory,
  }),
  {
    pushState: push,
    loadHistory: authActions.loadHistory
  }
)
export default class Home extends Component {
  static propTypes = {
    location: PropTypes.object,
    intl: intlShape.isRequired,
    pushState: PropTypes.func,
    user: PropTypes.object,
    history: PropTypes.array,
    loadedHistory: PropTypes.bool,
    loadHistory: PropTypes.func
  };

  state = {
    isHide: true,
    dataSource: []
  };

  componentDidMount() {
    const {user, loadHistory} = this.props;
    if (window) window.addEventListener('scroll', this.showNavigation);
    if (user) {
      loadHistory();
    }
  }

  componentWillUnmount() {
    if (window) window.removeEventListener('scroll', this.showNavigation);
  }

  showNavigation = () => {
    const {isHide} = this.state;
    const scrollY = window.pageYOffset || document.documentElement.scrollTop;
    if (scrollY > 600 && isHide) {
      this.setState({isHide: false});
    } else if (scrollY < 600 && !isHide) {
      this.setState({isHide: true});
    }

    this.prev = scrollY;
  };

  renderAuthHistory = () => {
    const {history, loadedHistory} = this.props;

    if (!loadedHistory) {
      return <ProgressBar />;
    }
    return (
      <div>
        <h2>Auth History</h2>
        <Table>
          <TableHeader
            adjustForCheckbox={false}
            displaySelectAll={false}
            enableSelectAll={false}
          >
            <TableRow>
              <TableHeaderColumn>
                <FormattedMessage
                  id="Home.Time"
                  defaultMessage="Time"
                />
              </TableHeaderColumn>
              <TableHeaderColumn>
                <FormattedMessage
                  id="Home.IP"
                  defaultMessage="IP"
                />
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false} showRowHover>
            {history && history.map((item, index) =>
              <TableRow key={index}>
                <TableRowColumn>
                  {item.createdAt && moment(item.createdAt).format('YYYY-MM-DD HH:mm:ss')}
                </TableRowColumn>
                <TableRowColumn>
                  {item.ip}
                </TableRowColumn>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
    );
  };

  render() {
    const {location, intl, user} = this.props;

    return (
      <div className="home">
        <Helmet title={intl.formatMessage(messages.Title)} />

        <AutoAffix viewportOffsetTop={0} container={this}>
          <div style={{zIndex: 500}}>
            <Navigation location={location} isLoginFormInPopUp={!this.state.isHide} />
          </div>
        </AutoAffix>

        <div className="container">
          <div className="col-xs-12">
            <h1>Home</h1>
            {user && this.renderAuthHistory()}
          </div>
        </div>
      </div>
    );
  }
}
