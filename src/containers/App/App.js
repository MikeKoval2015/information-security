import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import { push } from 'react-router-redux';
import config from '../../config';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { isLoaded as isAuthLoaded, load as loadAuth, logout } from 'redux/modules/auth';
import { asyncConnect } from 'redux-async-connect';
import {
  deepPurple800,
  deepPurple900,
  grey100, grey300, grey400, grey500,
  white, darkBlack, transparent,
} from 'material-ui/styles/colors';
import Navigation from './Navigation';
import {injectIntl, intlShape} from 'react-intl';


const muiTheme = getMuiTheme({
  fontFamily: 'Lato-Regular',
  palette: {
    primary1Color: '#614261',
    primary2Color: deepPurple800,
    primary3Color: grey400,
    accent1Color: deepPurple900,
    accent2Color: grey100,
    accent3Color: grey500,
    textColor: darkBlack,
    alternateTextColor: white,
    canvasColor: white,
    borderColor: grey300,
    pickerHeaderColor: '#614261',
    shadowColor: transparent,
  },
  // userAgent: 'all'
});

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    if (!isAuthLoaded(getState())) {
      promises.push(dispatch(loadAuth()));
    }

    return Promise.all(promises);
  }
}])
@injectIntl
@connect(
  state => ({user: state.auth.user, loggedOut: state.auth.loggedOut}),
  {pushState: push, logout, loadAuth})
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    user: PropTypes.object,
    pushState: PropTypes.func.isRequired,
    loadAuth: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    location: PropTypes.object,
    intl: intlShape.isRequired
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  static childContextTypes = {
    muiTheme: PropTypes.object
  };

  componentWillReceiveProps(nextProps) {
    if (!this.props.user && nextProps.user) {
      this.props.pushState('/');
    } else if (this.props.user && !nextProps.user) {
      if (nextProps.loggedOut) {
        this.props.pushState('/login');
        return;
      }
      this.setState({
        sessionExpired: true
      });
    }
  }

  render() {
    const {location} = this.props;
    const styles = require('./App.scss');
    const isHomePage = location && location.pathname === '/';
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div className={styles.app}>
          <Helmet {...config.app.head}/>
          {!isHomePage && <Navigation fixedTop location={location} />}
          <div className={styles.appContent} style={!isHomePage ? {marginTop: 66, minHeight: 'calc(100vh - 216px)'} : {}}>
            {this.props.children}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}
