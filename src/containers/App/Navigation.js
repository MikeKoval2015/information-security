import React, { Component, PropTypes } from 'react';
import { IndexLink } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import Navbar from 'react-bootstrap/lib/Navbar';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import NavDropdown from 'react-bootstrap/lib/NavDropdown';
import MenuItem from 'react-bootstrap/lib/MenuItem';
import {FormattedMessage, intlShape, injectIntl, defineMessages} from 'react-intl';
import SelectField from 'material-ui/SelectField';
import MUIMenuItem from 'material-ui/MenuItem';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import cookie from 'react-cookie';
import { logout } from 'redux/modules/auth';
import LoginForm from '../Login/LoginForm';
import ForgotPassword from '../ForgotPassword/ForgotPassword';
import {Popover as BootstrapPopover, OverlayTrigger} from 'react-bootstrap';
import ExpandMore from 'material-ui/svg-icons/navigation/expand-more';
import ExpandLess from 'material-ui/svg-icons/navigation/expand-less';
import Exit from 'material-ui/svg-icons/action/exit-to-app';

const messages = defineMessages({
  Profile: {
    id: 'App.Profile',
    defaultMessage: 'Profile'
  },
  MyFavorites: {
    id: 'App.MyFavorites',
    defaultMessage: 'My Favorites'
  },
  LogOut: {
    id: 'App.LogOut',
    defaultMessage: 'Logout'
  },
});

@injectIntl
@connect(
  state => ({user: state.auth.user, routing: state.routing}),
  {pushState: push, logout}
)
export default class Navigation extends Component {
  static propTypes = {
    user: PropTypes.object,
    pushState: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    fixedTop: PropTypes.bool,
    location: PropTypes.object,
    intl: intlShape.isRequired,
    isLoginFormInPopUp: PropTypes.bool
  };

  static defaultProps = {
    isLoginFormInPopUp: true
  };

  state = {
    open: false,
    showLoginForm: false
  };

  componentWillMount() {
    this.setState({
      locale: cookie.load('locale')
    });
  }

  handleLogout = () => {
    // event.preventDefault();
    this.props.logout();
  };

  changeLocale = (event, key, locale) => {
    cookie.save('locale', locale);
    if (window && document) {
      window.location = document.URL;
    }
  };

  handleChange = (event, index, value) => this.setState({value});

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();
    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };

  render() {
    const styles = require('./App.scss');
    const {user, fixedTop, intl, isLoginFormInPopUp, location} = this.props;
    const {showForgotPasswordModal} = this.state;
    const isLoginPage = location && location.pathname === '/login';

    const popoverBottom = (
      <BootstrapPopover id="popover-positioned-bottom" positionTop={200} style={{padding: 0, border: 'none', width: 360, boxShadow: 'none'}}>
        {!showForgotPasswordModal && <LoginForm isPopUp onForgotPasswordClick={() => this.setState({showForgotPasswordModal: true})} />}
        {showForgotPasswordModal && <ForgotPassword onLoginClick={() => this.setState({showForgotPasswordModal: false})} />}
      </BootstrapPopover>
    );

    return (
      <div>
        <Navbar fixedTop={fixedTop}>
          <Navbar.Header>
            <Navbar.Brand>
              <IndexLink to="/" activeStyle={{color: '#ffb900'}}>
                <div className={styles.brand}/>
              </IndexLink>
            </Navbar.Brand>
            <Navbar.Toggle/>
          </Navbar.Header>

          <Navbar.Collapse>
            <Nav navbar pullRight>
              <NavItem className="separator" />
              <LinkContainer to="/message">
                <NavItem>
                  <FormattedMessage
                    id="App.Message"
                    defaultMessage="Message"
                  />
                </NavItem>
              </LinkContainer>
              <NavItem className="separator" />
              {user && <NavDropdown className="profileDropdown" id="profileDropdown" title={
                <div style={{display: 'flex', alignItems: 'baseline'}}>
                  {user.isProfileImageExist && <img src={`/images/profiles/${user._id}.png`} width="24" height="24" style={{borderRadius: '50%', marginRight: 10}} />}
                  {!user.isProfileImageExist && <img src="/images/default_profile_image.png" width="24" height="24" style={{borderRadius: '50%', marginRight: 10}} />}
                  <span style={{display: 'flex', alignSelf: 'center'}}>
                      <span>
                        {user.name && user.name.first && user.name.last ? user.name.first + ' ' + user.name.last : user.email }
                        &nbsp;
                      </span>
                    <ExpandMore color="#a4a4a4" className="profileDropdownArrowDown" />
                    <ExpandLess color="#a4a4a4" className="profileDropdownArrowUp" />
                    </span>
                </div>
              } noCaret>
                <MenuItem onClick={() => this.handleLogout()}>
                  <div style={{display: 'flex', flexDirection: 'row'}}>
                    <span>{intl.formatMessage(messages.LogOut)}</span>
                    <Exit color="#a4a4a4" style={{width: 14, marginLeft: 5}} />
                  </div>
                </MenuItem>
              </NavDropdown>}
              {user && <NavItem className="separator" />}
              {!user && <LinkContainer to="/registration">
                <NavItem className="coloredNavItem">
                  <FormattedMessage
                    id="App.SignUp"
                    defaultMessage="Sign up"
                  />
                </NavItem>
              </LinkContainer>}
              {!user && <NavItem className="separator" />}
              {!isLoginPage && !user && isLoginFormInPopUp &&
              <OverlayTrigger rootClose trigger="click" placement="bottom" overlay={popoverBottom} container={fixedTop ? null : this} onEnter={() => this.setState({loginModalOpen: true})} onExit={() => this.setState({loginModalOpen: false})}>
                <NavItem className="coloredNavItem" active={this.state.loginModalOpen}>
                  <FormattedMessage
                    id="App.LogIn"
                    defaultMessage="Log In"
                  />
                </NavItem>
              </OverlayTrigger>}
              {!isLoginPage && !user && !isLoginFormInPopUp &&
              <LinkContainer to="/login">
                <NavItem className="coloredNavItem" active={this.state.loginModalOpen}>
                  <FormattedMessage
                    id="App.LogIn"
                    defaultMessage="Log In"
                  />
                </NavItem>
              </LinkContainer>
              }
              {!isLoginPage && !user && !isLoginFormInPopUp && <NavItem className="separator" />}
              <NavItem className="langNavItem">
                <SelectField
                  value={this.state.locale}
                  onChange={this.changeLocale}
                  autoWidth
                  style={{width: 50}}
                  labelStyle={{color: '#7d7d7d', fontSize: 14, fontFamily: 'Lato-Bold'}}
                  underlineStyle={{display: 'none'}}
                  iconStyle={{display: 'none'}}
                >
                  <MUIMenuItem value="en-US" primaryText="En" />
                  <MUIMenuItem value="de-DE" primaryText="De" />
                </SelectField>
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}
