import * as fs from 'fs';
import {sync as globSync} from 'glob';
import {sync as mkdirpSync} from 'mkdirp';
import Translator from './lib/translator';
import _ from 'lodash';

const MESSAGES_PATTERN = './static/messages/**/*.json';
const LANG_DIR         = './static/lang/';

// Aggregates the default messages that were extracted from the example app's
// React components via the React Intl Babel plugin. An error will be thrown if
// there are messages in different components that use the same `id`. The result
// is a flat collection of `id: message` pairs for the app's default locale.
let defaultMessages = globSync(MESSAGES_PATTERN)
    .map((filename) => fs.readFileSync(filename, 'utf8'))
    .map((file) => JSON.parse(file))
    .reduce((collection, descriptors) => {
        descriptors.forEach(({id, defaultMessage}) => {
            if (!collection.hasOwnProperty(id)) {
              collection[id] = defaultMessage;
            }
        });

        return collection;
    }, {});

// For the purpose of this example app a fake locale: `en-UPPER` is created and
// the app's default messages are "translated" into this new "locale" by simply
// UPPERCASING all of the message text. In a real app this would be through some
// offline process to get the app's messages translated by machine or
// processional translators.
// let uppercaseTranslator = new Translator((text) => text.toUpperCase());
// let uppercaseMessages = Object.keys(defaultMessages)
//     .map((id) => [id, defaultMessages[id]])
//     .reduce((collection, [id, defaultMessage]) => {
//         collection[id] = uppercaseTranslator.translate(defaultMessage);
//         return collection;
//     }, {});
/*
let originFrMessages = fs.existsSync(LANG_DIR + 'fr-FR.json') ? JSON.parse(fs.readFileSync(LANG_DIR + 'fr-FR.json')) : {};
let frMessages = Object.keys(defaultMessages)
  .map((id) => [id, defaultMessages[id]])
  .reduce((collection, [id, defaultMessage]) => {
    collection[id] = originFrMessages[id] || defaultMessage;
    return collection;
  }, {});

frMessages = _.fromPairs(_.sortBy(_.toPairs(frMessages), item => item[0]));


let originNdMessages = fs.existsSync(LANG_DIR + 'nd-ND.json') ? JSON.parse(fs.readFileSync(LANG_DIR + 'nd-ND.json')) : {};
let ndMessages = Object.keys(defaultMessages)
  .map((id) => [id, defaultMessages[id]])
  .reduce((collection, [id, defaultMessage]) => {
    collection[id] = originNdMessages[id] || defaultMessage;
    return collection;
  }, {});

ndMessages = _.fromPairs(_.sortBy(_.toPairs(ndMessages), item => item[0]));
*/
let originDeMessages = fs.existsSync(LANG_DIR + 'de-DE.json') ? JSON.parse(fs.readFileSync(LANG_DIR + 'de-DE.json')) : {};
let deMessages = Object.keys(defaultMessages)
  .map((id) => [id, defaultMessages[id]])
  .reduce((collection, [id, defaultMessage]) => {
    collection[id] = originDeMessages[id] || defaultMessage;
    return collection;
  }, {});

deMessages = _.fromPairs(_.sortBy(_.toPairs(deMessages), item => item[0]));
/*
let originItMessages = fs.existsSync(LANG_DIR + 'it-IT.json') ? JSON.parse(fs.readFileSync(LANG_DIR + 'it-IT.json')) : {};
let itMessages = Object.keys(defaultMessages)
  .map((id) => [id, defaultMessages[id]])
  .reduce((collection, [id, defaultMessage]) => {
    collection[id] = originItMessages[id] || defaultMessage;
    return collection;
  }, {});

deMessages = _.fromPairs(_.sortBy(_.toPairs(deMessages), item => item[0]));

*/

mkdirpSync(LANG_DIR);
fs.writeFileSync(LANG_DIR + 'en-US.json', JSON.stringify(defaultMessages, null, 2));
//fs.writeFileSync(LANG_DIR + 'fr-FR.json', JSON.stringify(frMessages, null, 2));
//fs.writeFileSync(LANG_DIR + 'nd-ND.json', JSON.stringify(ndMessages, null, 2));
fs.writeFileSync(LANG_DIR + 'de-DE.json', JSON.stringify(deMessages, null, 2));
//fs.writeFileSync(LANG_DIR + 'it-IT.json', JSON.stringify(itMessages, null, 2));
