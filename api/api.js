import config from '../src/config';
import app from 'config/express';

if (config.apiPort) {
  app.listen(config.apiPort, (err) => {
    if (err) {
      console.error(err);
    }
    console.info('----\n==> 🌎  API is running on port %s', config.apiPort);
    console.info('==> 💻  Send requests to http://%s:%s', config.apiHost, config.apiPort);
  });

} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}
