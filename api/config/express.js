import express from "express";
import session from "express-session";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import helmet from "helmet";
import hpp from "hpp";
import httpStatus from "http-status";
import expressValidation from "express-validation";
import routes from "../server/routes";
import Module from "../server/models";
import config from "../../src/config";
import apiConfig from "./config";
import APIError from "../server/helpers/APIError";
import {ping} from "express-ping";
import async from "async";
import mongoose from "mongoose";
import passport from "passport";
import LocalStrategy from "passport-local";
import _ from "lodash";
import User from "../server/models/user";
import logger from "./logger";
const MongoStore = require('connect-mongo')(session);

const Strategy = LocalStrategy.Strategy;
const app = express();

app.config = apiConfig;
app.logger = logger;

app.use(function(req, res, next) {
	var hostname = req.hostname.replace('http://', '');
	var allowedOrigins = ['http://'+hostname+':3000',
		'http://'+hostname+':8080',
		'http://'+hostname+':80',
		'http://'+hostname+':3030',
		'http://'+hostname+'/',
	];

	var origin = req.headers.origin;

	if(allowedOrigins.indexOf(origin) > -1){
		res.setHeader('Access-Control-Allow-Origin', origin);
	}
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, X-AUTHENTICATION, X-IP, Content-Type, Accept');
	res.header('Access-Control-Allow-Credentials', true);
	res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
	next();
});

passport.use('local-signup', new Strategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, User.signup.bind(User)));

passport.use('local-login', new Strategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
}, User.login.bind(User)));

passport.serializeUser(function(user, cb) {
	cb(null, user._id);
});

passport.deserializeUser(function(id, cb) {
	User.findById(id, function (err, user) {
		if (err) { return cb(err); }
		cb(null, user);
	});
});


// parse body params and attache them to req.body
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(helmet.xssFilter());
app.use(helmet.noCache());
app.use(helmet.noSniff());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.referrerPolicy());
app.use(helmet.ieNoOpen());
//app.use(helmet.contentSecurityPolicy());
app.use(helmet.dnsPrefetchControl());
app.use(hpp());

app.use(cookieParser('nyan cat'));
const sessionOpts = {
	saveUninitialized: true,
	resave: true,
	store: new MongoStore({ mongooseConnection: mongoose.connection, stringify: false }),
	secret: 'nyan cat',
	cookie : { maxAge : 2419200000},
};
app.use(session(sessionOpts));

app.use(passport.initialize());
app.use(passport.session());

app.options('*', function(req, res) {
	res.status(200).end();
});

// app.use(compress());
// app.use(methodOverride());
app.use(ping());

// disable 'X-Powered-By' header in response
app.disable('x-powered-by');

// enable CORS - Cross Origin Resource Sharing
// app.use(cors());


app.models = {};
app.modules = {
	modules: new Module(app),
	each: function(callFunc) {
		_.forEach(app.modules, function(obj, name) {
			if (typeof obj == 'object') {
				callFunc(obj);
			}
		});
	}
};
app.modules.each(function(moduleObj) {
	moduleObj.initModels();
});

app.services = {
	mail: require('../server/services/mail'),
};

async.series([
	_.partial(async.parallel, [
		_.partial(app.services.mail.init, app)
	]),
	function (next) {
		console.log('Connecting to mongodb...');
		mongoose.connect(apiConfig.mongodb, next);

		mongoose.connection.on('error', function (err) {
			console.log(err);
			// app.logger.error(err);
		});
		mongoose.set('debug', false);
	},
	function (next) {
		console.log('Connected to mongodb successfully');
		next();
	},
	// function (next) {
	//   require('./migrations').migrateToActual(app, next);
	// },
	function () {
		console.log('Http server starting at', config.apiPort, '...');
	},
	function (next) {
		// app.modules.each(function(moduleObj) {
		//   if (moduleObj.initServer) {
		//     moduleObj.initServer();
		//   }
		// });
		next();
	}

], function (err) {
	if (err) { console.log(err)/*app.logger.error(err);*/ }
});

//
// // enable detailed API logging in dev env
// if (config.env === 'development') {
// 	expressWinston.requestWhitelist.push('body');
// 	expressWinston.responseWhitelist.push('body');
// 	app.use(expressWinston.logger({
// 		winstonInstance,
// 		meta: true, 	// optional: log meta data about request (defaults to true)
// 		msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
// 		colorStatus: true 	// Color the status code (default green, 3XX cyan, 4XX yellow, 5XX red).
// 	}));
// }

// mount all routes on /api path
app.use('/', routes);

// if error is not an instanceOf APIError, convert it.
app.use((err, req, res, next) => {
	if (err instanceof expressValidation.ValidationError) {
		// validation error contains errors which is an array of error each containing message[]
		const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ');
		const error = new APIError(unifiedErrorMessage, err.status);
		return next(error);
	} else if (!(err instanceof APIError)) {
		const apiError = new APIError(err.message, err.status, err.isPublic);
		return next(apiError);
	}
	return next(err);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
	const err = new APIError('API not found', httpStatus.NOT_FOUND);
	return next(err);
});

// log error in winston transports except when executing test suite

// error handler, send stacktrace only during development
app.use((err, req, res, next) => {
	req.app.logger.error('GENERAL', req.path, 'error', err.message);
	let json = {};
	json = (err.isPublic && typeof err.message === 'object') ? err.message : {
		_error: err.isPublic ? err.message : httpStatus[err.status],
		// stack: config.env === 'development' ? err.stack : {}
	};
	res.status(err.status).json(json);
});

export default app;
