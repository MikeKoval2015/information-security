export default {
  'mail': {
    service: 'Gmail',
    auth: {
      user: 'technical.data21@gmail.com',
      pass: 'p5nOKVkV'
    },
    from: 'technical.data21@gmail.com'
  },
  feedbackEmail: 'technical.data21@gmail.com',
  pagination: {
    authLogsPerPage: 20
  },
  limits: {
    login: {
      windowMs: 60 * 1000, // 1 minute
      max: 3,
      delayMs: 0,
      message: 'Too many attempts to login with wrong credentials, please try again after a minute'
    }
  },
  mongodb: 'mongodb://localhost:27017/information_security',
  siteUrl: 'http://localhost:3000',
}