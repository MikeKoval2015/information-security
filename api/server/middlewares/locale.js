import {sync as globSync} from 'glob';
import {readFileSync} from 'fs';
import path from 'path';

const translations = globSync('./static/lang/*.json')
  .map((filename) => [
    path.basename(filename, '.json'),
    readFileSync(filename, 'utf8'),
  ])
  .map(([locale, file]) => [locale, JSON.parse(file)])
  .reduce((collection, [locale, messages]) => {
    collection[locale] = messages;
    return collection;
  }, {});

export default function (req, res, next) {
  const locale = req.cookies.locale || process.env.LOCALE || 'en-US';
  const messages = translations[locale];

  req.locale = {
    locale,
    messages
  };

  req.translate = (id, section) => (messages && messages['API.' + section + '.' + id]) || id;

  next();
}