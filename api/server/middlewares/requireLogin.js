import APIError from "../helpers/APIError";
import httpStatus from "http-status";

export default function (req, res, next) {
  if (!req.user) {
    return next(new APIError('Authorization is required', httpStatus.UNAUTHORIZED, true));
  }

  next();
}