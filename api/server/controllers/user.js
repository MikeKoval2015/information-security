﻿import mongoose from "mongoose";
import async from "async";
import passport from "passport";
import moment from "moment";
import APIError from "../helpers/APIError";
import httpStatus from "http-status";

function register(req, res, next) {
    const User = mongoose.model('User', req.app.models.user.schema);

    async.auto({
        'similarUser': (next) => {
            if (!req.body.email || !req.body.password) {
                return next({success: false, message: 'e-mail and password are mandatory fields' });
            }

            User.findOne({email: req.body.email}, (err, data) => {
                if (err) return next(err);
                if (data) {
                    return res.json({ email: 'This email is taken', _error: 'Sign up failed!' });
                }

                return next();
            });
        },
        'user': ['similarUser', (data, next) => {
            const user = new User(req.body);
            user.password = user.generateHash(req.body.password);
            user.username = user.email;
            user.activationToken = Math.random().toString(36).slice(-8);

            user.save(function (err, data) {
                if (err) {
                    return res.json({ success: false, message: 'Something went wrong while registering' }); //TODO: Better error handling
                }

                next(null, data);
            });
        }],
        'mail': ['user', (data, next) => {
            const {user} = data;
            const siteUrl = req.app.config.siteUrl;
            const welcomeText = req.translate('WelcomeText', 'Users');
            const loginPageTitle = req.translate('LoginPageTitle', 'Users');
            const subject = req.translate('RegisterSubject', 'Users');
            req.app.services.mail.sendTemplate('register', user.email, {user, siteUrl, welcomeText, loginPageTitle, subject}, next);
        }]
    }, function (err, data) {
        if(err) {
            err = new APIError(err, httpStatus.INTERNAL_SERVER_ERROR);
            return next(err);
        }

        res.json({ success: true });
    })

}

function login(req, res, next) {
    passport.authenticate('local-login', function(err, user, info) {
        if (err) return next(err);

        if (!user) {
            return res.status(200).json({
                user: null,
                info: {
                    message: 'Failed to login'
                }
            });
        }

        req.logIn(user, function(err) {
            if (err) {
                return next(err);
            }
        });

      res.status(200).json({
        user: user,
        info: info
      });
    })(req, res, next);
}

function forgot_password (req, res) {
    const User = mongoose.model('User', req.app.models.user.schema);
    const Token  = mongoose.model('ResetPasswordToken', req.app.models.resetPasswordToken.schema);

    async.auto({
        'user': (next) => {
            User.findOne({email: req.body.email}, next);
        },
        'token': ['user', (data, next) => {
            if (!data.user) {
                return next({code: 500, msg: 'User with this email (' + req.body.email +') was not found'});
            }

            const user = data.user;
            const t = Math.random().toString(36).slice(-8);
            const token = new Token({
                userId: user._id,
                token: t
            });

            token.save(next);
        }],
        'mail': ['user', 'token', (data, next) => {
            const user = data.user;
            const token = data.token[0];
            const siteUrl = req.app.config.siteUrl;
            const linkToChangePasswordTitle = req.translate('LinkToChangePasswordTitle', 'Users');
            req.app.services.mail.sendTemplate('forgetPassword', user.email, {token: token.token, siteUrl, linkToChangePasswordTitle}, next);
        }]
    }, function (err, data) {
        // if(err) {
        //     return res.status(err.code || 500).send(err);
        // }

        res.json({});
    });
}

function change_password (req, res, next) {
    const User = mongoose.model('User', req.app.models.user.schema);
    const Token  = mongoose.model('ResetPasswordToken', req.app.models.resetPasswordToken.schema);

    const yesterday = moment().subtract(1, 'day')._d;
    async.auto({
        'token': (next) => {
            Token.findOne({token: req.body.token, createDate: {$gte: yesterday}}, next).sort({creationDate: -1});
        },
        'user': ['token', (data, next) => {
            const token = data.token;

            if (!token) {
                return next({code: 500, msg: 'token is not found or expired'})
            }

            User.findById(token.userId, next);
        }],
        'updatedUser': ['user', (data, next) => {
            const user = data.user;

            user.password = user.generateHash(req.body.password);

            user.save(next);
        }],
        'removeToken': ['token', (data, next) => {
            const token = data.token;

            token.remove(next);
        }]
    }, function (err, data) {
        if(err) {
            err = new APIError(err, httpStatus.INTERNAL_SERVER_ERROR);
            return next(err);
        }

        res.json(data);
    });
}

function getResetPasswordToken (req, res, next) {
    const User = mongoose.model('User', req.app.models.user.schema);
    const Token  = mongoose.model('ResetPasswordToken', req.app.models.resetPasswordToken.schema);

    const yesterday = moment().subtract(1, 'day')._d;
    async.auto({
        'token': (next) => {
            Token.findOne({token: req.params.token, createDate: {$gte: yesterday}}, next).sort({creationDate: -1});
        },
        'user': ['token', (data, next) => {
            const token = data.token;

            if (!token) {
                return next({code: 500, msg: 'token is not found or expired'})
            }

            User.findById(token.userId, {name: true}, next);
        }]
    }, function (err, data) {
        if(err) {
            err = new APIError(err, httpStatus.INTERNAL_SERVER_ERROR);
            return next(err);
        }

        res.json(data.user);
    });
}

function activate (req, res, next) {
  const User = mongoose.model('User', req.app.models.user.schema);
  const activationToken = req.params.token;

  async.auto({
    'user': next => {
        User.findOne({status: 'not_activated', activationToken}, {_id: true}, next);
    },
    'activate': ['user', ({user}, next) => {
        if (!user) {
          return next({code: 500, msg: 'Token is wrong or user is already activated'})
        }
        user.status = 'activated';
        user.save(next);
    }]
  }, function (err, {user}) {
    if(err) {
      err = new APIError(err, httpStatus.INTERNAL_SERVER_ERROR);
      return next(err);
    }

    res.json(user);
  });
}

function isEmailExists (req, res, next) {
    const User = mongoose.model('User', req.app.models.user.schema);
    const params = {email: req.query.email};
    if (req.user) {
        params._id = {$ne: req.user._id};
    }

    async.auto({
        'user': (next) => {
            User.findOne(params, next)
        }
    }, function (err, data) {
        if(err) {
            err = new APIError(err, httpStatus.INTERNAL_SERVER_ERROR);
            return next(err);
        }

        res.json(!!data.user);
    });
}

function getAuthHistory (req, res, next) {
    const AuthHistory = mongoose.model('AuthHistory', req.app.models.authHistory.schema);

    async.auto({
        'items': (next) => {
            AuthHistory.find({userId: req.user._id}, next).sort({createdAt: -1})
        }
    }, function (err, {items}) {
        if(err) {
            err = new APIError(err, httpStatus.INTERNAL_SERVER_ERROR);
            return next(err);
        }

        res.json(items);
    });
}

export default { register, login, forgot_password, change_password, getResetPasswordToken, isEmailExists, activate, getAuthHistory };
