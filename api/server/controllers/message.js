﻿import mongoose from "mongoose";
import rsa from '../helpers/rsa';

function list(req, res, next) {
    const Message = mongoose.model('Message', req.app.models.message.schema);
    Message.find({}).sort({date: -1})
      .then(messages => {
          return res.json(messages);
      }).catch(function (err) {
        console.error(err);
        next(err);
    })
}

function send(req, res, next) {
    const Message = mongoose.model('Message', req.app.models.message.schema);
    const decryptedNumbers = [];
    for(let e of req.body.encrypted.split('|')){
        decryptedNumbers.push(rsa.decrypt(req.keys.private, e));
    }
    let message = rsa.toStringArray(decryptedNumbers);
    new Message( { text:  message, date: new Date()} )
      .save((err, doc) => {
          if(err) return console.error(err);
          res.json(doc);
      });
}

function keys(req, res) {
  return res.json(req.keys.public);
}

export default { list, send, keys };
