import express from 'express';
import userRoutes from './user';
import messageRoutes from './message';

import localeMiddleware from '../middlewares/locale';


const router = express.Router();	// eslint-disable-line new-cap

router.use(localeMiddleware);

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
		res.send('OK')
);

// mount user routes at /users
router.use('/users', userRoutes);

router.use('/messages', messageRoutes);

export default router;

