﻿import express from 'express';
import userCtrl from '../controllers/user';
import requireLogin from '../middlewares/requireLogin';
import RateLimit from 'express-rate-limit';
const router = express.Router();	// eslint-disable-line new-cap
import _ from 'lodash';
import config from '../../config/config';

const loginLimiter = new RateLimit(config.limits.login);

router.route('/login')
    .post(loginLimiter)
    .post(userCtrl.login);

router.route('/login')
    .get((req, res) => {
      const user = req.user && req.user._doc && _.assign({}, req.user._doc);
      res.json({ user });
    });

router.post('/logout', function(req, res) {
    // req.session.user = null;
    req.logout();
    res.json({ok: true});
});

router.route('/register')
   .post(userCtrl.register);

router.route('/forgot_password')
  .post(userCtrl.forgot_password);

router.route('/change_password')
  .post(userCtrl.change_password);

router.route('/get_reset_password_token/:token')
  .get(userCtrl.getResetPasswordToken);

router.route('/email_exists')
  .get(userCtrl.isEmailExists);

router.route('/activate/:token')
  .post(userCtrl.activate);

router.route('/auth_history')
  .get(requireLogin)
  .get(userCtrl.getAuthHistory);

export default router;
