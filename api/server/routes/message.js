﻿import express from 'express';
import messageCtrl from '../controllers/message';
const router = express.Router();	// eslint-disable-line new-cap
import rsa from '../helpers/rsa';
const keys = rsa.generateKeys();

router.use((req, res, next) => {
  req.keys = keys;
  next();
});

router.route('/')
    .get(messageCtrl.list);

router.route('/keys')
  .get(messageCtrl.keys);

router.route('/')
    .post(messageCtrl.send);

export default router;
