import bigInt from 'big-integer';

function getRandomPrime(min = 1, max = 9999){
    let res;
    do {
        res = bigInt.randBetween(min, max);
    } while(!res.isProbablePrime());
    return res;
}

function xgcd(a,b) {
    if (b.equals(0)) {
        return [bigInt(1), bigInt(0), a]
    }
    else {
        const temp = xgcd(b, a.mod(b));
        const x = temp[0];
        const y = temp[1];
        const d = temp[2];
        return [y, x.subtract(y.multiply(Math.floor(a.divide(b)))), d];
    }
}

const rsa = {
    generateKeys: (max = 999999) => {
        let p, q, n, phi, e, d;

        p = getRandomPrime(3, max);
        do { q = getRandomPrime(3, (max) + 1);  }
        while(q.equals(p));

        n = p.multiply(q);

        phi = p.subtract(1).multiply(q.subtract(1));

        // e = bigInt(65537);
        do{
            e = getRandomPrime(1, phi);
        }
        while(!xgcd(e, phi)[2].equals(1));
        d = e.modInv(phi);

        return {
            public: { e: e, n: n },
            private: { d: d, n: n }
        };
    },

    encrypt: ( { e: e, n: n }, number) =>
      bigInt(number).modPow(e, n),
    decrypt: ({ d: d, n: n }, number) =>
        bigInt(number).modPow(d, n),

    // create a character array from a string
    toCharArray(input){
        const charArray = [];
        // split string to character array
        input.split('').forEach((character) => {
            charArray.push(character.charCodeAt(0)); // save each char as its corresponding ASCII number
        });
        // return array of characters represented as its ASCII number
        return charArray;
    },

    // create a string from a character array
    toStringArray(input){
        const stringArray = [];
        // convert number to ASCII representation
        input.forEach(function(asciiChar){
            stringArray.push(String.fromCharCode(asciiChar)); // add single char to array
        });
        return stringArray.join(''); // combine characters to string
    }
};




export default rsa;