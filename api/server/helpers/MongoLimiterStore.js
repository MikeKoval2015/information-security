import RateLimit from '../models/rateLimit';
import async from 'async';

function MemoryStore(windowMs, type) {
  this.incr = function(key, cb) {
    async.auto({
      'item': (next) => {
        RateLimit.findOne({type, key}, next);
      },
      'insert': ['item', (data, next) => {
        const item = data.item;
        if (item) return next();
        const rateLimit = new RateLimit({type, key});
        rateLimit.save(next);
      }],
      'update': ['item', (data, next) => {
        RateLimit.update({type, key}, {$inc: {value: 1}}, next);
      }],
      'result': ['item', 'insert', 'update', (data, next) => {
        RateLimit.findOne({type, key}, (err, data) => {
          if (err) return next(err);
          return next(null, data.value);
        });
      }],
    }, (err, data) => {
      if (err) return cb(err);
      return cb(null, data.result);
    });
  };

  this.resetAll = function() {
    async.auto({
      'item': (next) => {
        RateLimit.remove({type}, next);
      },
    });
  };

  // export an API to allow hits from one or all IPs to be reset
  this.resetKey = function(key) {
    async.auto({
      'item': (next) => {
        RateLimit.remove({type, key}, next);
      },
    });
  };

  // simply reset ALL hits every windowMs
  setInterval(this.resetAll, windowMs).unref();
}

module.exports = MemoryStore;