'use strict';

var nodemailer = require('nodemailer'),
  path = require('path'),
  fs = require('fs'),
  async = require('async'),
  minify = require('html-minifier').minify,
  cssmin = require('cssmin'),
  juice = require('juice'),
  handlebars = require('handlebars'),
  _ = require('lodash');
import smtpTransport from 'nodemailer-smtp-transport';

var transporter;
var app;

var htmlMinOpts = {
  collapseBooleanAttributes: true,
  collapseWhitespace: true,
  conservativeCollapse: true,
  removeAttributeQuotes: true,
  removeComments: true,
  removeEmptyAttributes: true,
  removeRedundantAttributes: true,
  removeScriptTypeAttributes: true,
  removeStyleLinkTypeAttributes: true
};

var juiceOpts = {
};
var controllers = {};

function registerPartials(next) {
  var partialPath = path.join(__dirname, 'partials');
  fs.readdir(partialPath, function (err, files) {
    if (err) { return next(err); }
    async.each(files, function (fileName, next) {
      app.logger.info('[MailSvc] loading partial "' + fileName + '"...');
      fs.readFile(path.join(partialPath, fileName), {encoding: 'utf8'}, function (err, data) {
        if (err) { return next(err); }
        if (path.extname(fileName) === '.css') {
          data = cssmin(data);
        }
        handlebars.registerPartial(fileName, data);
        app.logger.info('[MailSvc] partial "' + fileName + '" loaded.');
        next();
      });
    }, next);
  });
}

function getTemplate(templateName, format, next) {
  var templatePath = path.join(__dirname, 'templates', templateName + '.' + format);
  fs.exists(templatePath, function (exists) {
    if (!exists) { return next(); }
    fs.readFile(templatePath, {encoding: 'utf8'}, function (err, data) {
      if (err) { return next(err); }
      next(null, handlebars.compile(data));
    });
  });
}

exports.init = function (_app, next) {
  app = _app;
  transporter = nodemailer.createTransport(smtpTransport({service: app.config.mail.service, auth: app.config.mail.auth}));
  app.logger.info('Mail service initialized "%s"' + app.config.mail.service);
  async.parallel([registerPartials], next);
};

exports.sendTemplate = function (name, email, options, cb) {
  const subject = options && options.subject || 'Notification';
  const from = options.from || app.config.mail.from;
  async.auto({
    htmlTemplate: _.partial(getTemplate, name, 'html'),
    txtTemplate: _.partial(getTemplate, name, 'txt'),
    sendOptions: function (next) {
      next(null, {
        from,
        to: email,
        subject,
        xMailer: 'smtp'
      });
    },
    model: ['sendOptions', function (data, next) {
      var model = _.clone(options);
      model.email = email;
      model.config = {
        url: app.config.url,
        urls: app.config.urls
      };
      var controller = controllers[name];
      if (!controller || typeof controller.controller !== 'function') { return next(null, model); }
      controller.controller(app, data.sendOptions, model, next);
    }],
    send: ['htmlTemplate', 'txtTemplate', 'sendOptions', 'model', function (data, next) {
      var send = function (next) {
        transporter.sendMail(data.sendOptions, function (err, info) {
          if (err) { return next(err); }
          app.logger.info('Message ' + name + ' sent to ' + email + ': ' + info.response);
          next();
        });
      };

      if (!data.txtTemplate && !data.htmlTemplate) {
        return next(app.logger.info('[MailSvc] Template "' + name + '" not found.'));
      }

      if (data.txtTemplate) {
        data.sendOptions.text = data.txtTemplate(data.model);
      }

      if (data.htmlTemplate) {
        var mailHtml = data.htmlTemplate(data.model);
        var html = juice(mailHtml, _.extend({url: app.config.url}, juiceOpts));//, function (err, html) {
        //  if (err) { return next(err); }
          mailHtml = minify(html, htmlMinOpts);
          data.sendOptions.html = mailHtml;
          send(next);
        //});
      } else {
        send(next);
      }
    }]
  }, function (err) {
    if (err) {
      if (typeof cb !== 'function') {
        return app.logger.error(err);
      } else {
        return cb(err);
      }
    }
    if (typeof cb === 'function') {
      return cb();
    }
  });
};
