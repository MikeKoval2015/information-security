import * as userModel from './user';
import * as resetPasswordTokenModel from './resetPasswordToken';
import * as sessionModel from './session';
import * as authHistory from './authHistory';
import * as messageModel from './message';

export default function (app) {
	this.app = app;

	this.initModels = function () {
    this.app.models.user = userModel;
		this.app.models.resetPasswordToken = resetPasswordTokenModel;
		this.app.models.session = sessionModel;
		this.app.models.authHistory = authHistory;
		this.app.models.message = messageModel;
	}
}