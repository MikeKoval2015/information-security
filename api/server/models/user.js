﻿import mongoose from 'mongoose';
import bcryptjs from 'bcryptjs';
import timestamps from 'mongoose-timestamp';
import passportLocalMongoose from 'passport-local-mongoose';
import Session from './session';
import AuthHistory from './authHistory';
import async from 'async';

const scheme = new mongoose.Schema({
    email: {
        type: { String, lowercase: true, required: true }
    },
    password: {
        type: { String, required: true }
    },
    role: {
        type: String,
        enum: ['Customer', 'Admin'],
        default: 'Customer'
    },
    status: {
      type: String,
      enum: ['not_activated', 'activated', 'terminated'],
      default: 'not_activated'
    },
    activationToken: {
        type: String, required: true
    }
},
{
    strict: true,
    safe: true,
    collection: 'user'
});

scheme.index({email: 1, username: 1}, {unique: true});

scheme.plugin(timestamps);
scheme.plugin(passportLocalMongoose);

scheme.methods.generateHash = function(password) {
    return bcryptjs.hashSync(password, bcryptjs.genSaltSync(8), null);
};

scheme.methods.validPassword = function(password) {
    return bcryptjs.compareSync(password, this.password);
};

scheme.statics.login = function(req, email, password, done) {
    this.findOne({
        'email': email,
        'status': 'activated'
    }, function(err, user) {
        if (err) return done(err);

        if (!user) {
            return done(null, false, {
                message: 'User not found'
            });
        }

        if (!user.validPassword(password)) {
            return done(null, false, {
                message: 'Wrong password'
            });
        }

        async.auto({
            'save': (next) => {
                user.save(next)
            },
            'removeSession': ['save', ({}, next) => {
              Session.remove({'session.passport.user': user._id}, next);
            }],
            'storeHistory': ['save', ({}, next) => {
              const ip = req.headers['x-forwarded-for'] ||
              req.connection.remoteAddress ||
              req.socket.remoteAddress ||
              req.connection.socket.remoteAddress;

              const authHistory = new AuthHistory({
                  userId: user._id,
                  ip
              });

              authHistory.save(next);
            }]
        }, (err) => {
            console.log('---err', err);
          if (err) return done(null, user, {
            message: 'Something went wrong'
          });
          return done(null, user, {
            message: 'Successfully logged in'
          });
        });
    });
};

scheme.statics.signup = function(req, email, password, done) {
    let UserModel = this;

    this.findOne({
        'email': email
    }, function(err, user) {
        if (err) return done(err);

        if (user) {
            return done(null, false, {
                message: 'This email is already in use'
            });
        }

        let newUser = new UserModel();
        newUser.email = email;
        newUser.password = newUser.generateHash(password);

        newUser.save(function(err, product) {
            if (err) return done(err);

            done(null, product, {
                message: 'Successfully signed up'
            });
        });
    });
};

export default mongoose.model('User', scheme);
