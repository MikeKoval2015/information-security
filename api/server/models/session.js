﻿import mongoose from 'mongoose';
const scheme = new mongoose.Schema({
   session: {type: Object}
},
{
    strict: true,
    safe: true,
    collection: 'sessions'
});

export default mongoose.model('Session', scheme);
