﻿import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

const schema = new mongoose.Schema({
    ip: {type: String, required: true },
    userId: {type: mongoose.Schema.Types.ObjectId, required: true}
},
{
    strict: true,
    safe: true,
    collection: 'authHistory'
});

schema.index({userId: 1});

schema.plugin(timestamps);

export default mongoose.model('AuthHistory', schema);
