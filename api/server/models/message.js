﻿import mongoose from 'mongoose';

const scheme = new mongoose.Schema({
    text: String,
    date: Date
},
{
    strict: true,
    safe: true,
    collection: 'message'
});

export default mongoose.model('Message', scheme);
